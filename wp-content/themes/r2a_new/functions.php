<?php
    /**
     * R2A new Theme functions
     *
     * @package WordPress
     * @subpackage intentionally-blank
     */


	/**
	 * Sets up theme defaults and registers the various WordPress features that
	 * this theme supports.
     *
     *
     *
     * Initializing R2A new custom theme
	 */
	function r2a_new_setup() {
		load_theme_textdomain( 'intentionally-blank' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );

		// This theme allows users to set a custom background.
		add_theme_support( 'custom-background', apply_filters( 'intentionally_blank_custom_background_args', array(
			'default-color' => 'f5f5f5',
		) ) );
// This theme uses wp_nav_menu() in one location.
        register_nav_menus( array(
            'top' => esc_html__( 'Top Menu', 'r2a-new' ),
        ) );
        add_theme_support( 'customize-selective-refresh-widgets' );

		add_theme_support( 'custom-logo' );
		add_theme_support( 'custom-logo', array(
			'height'      => 256,
			'width'       => 256,
			'flex-height' => true,
			'flex-width'  => true,
			'header-text' => array( 'site-title', 'site-description' ),
		) );

		/**
		 * Sets up theme defaults and registers the various WordPress features that
		 * this theme supports.
		 */
		function r2a_new_custom_logo() {
			if ( function_exists( 'the_custom_logo' ) ) {
				the_custom_logo();
			}
		}
	}
add_action( 'after_setup_theme', 'r2a_new_setup' );

add_action( 'customize_register', function( $wp_customize ) {
	$wp_customize->remove_section( 'static_front_page' );
} );
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function r2a_new_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'r2a-test' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'r2a-test' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'r2a_new_widgets_init' );