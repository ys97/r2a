<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo get_theme_root_uri()?>/test/assets/css/style.css">
    <link rel="stylesheet" href="<?php echo get_theme_root_uri()?>/test/assets/css/animate.css">
    <link rel="stylesheet" href="<?php echo get_theme_root_uri()?>/test/assets/css/owl.min.css">
    <link rel="stylesheet" href="<?php echo get_theme_root_uri()?>/test/assets/css/owl.theme.css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="<?php echo get_theme_root_uri()?>/test/assets/js/test_script.js" type="text/javascript"></script>
    <script src="<?php echo get_theme_root_uri()?>/test/assets/js/owl.min.js" type="text/javascript"></script>
    <title>Pinnacle</title>
</head>
<body>
<section class="bg-default">
    <div class="container">
        <div class="row p-2">
            <div class="col-lg-6 col-8">
                <div class="row">
                    <a href="tel:(319)3930900" class="blue ml-3 mr-3"><span class="lighter_blue">(319)</span>393-0900</a>
                    <a href="mailto:info@pinnaclerealtyia.com" class="blue ml-3 mr-3"><span class="lighter_blue">info@</span>pinnaclerealtyia.com</a>
                </div>
            </div>
            <div class="col-lg-6 col-4">
                <div class="row justify-content-around">
                    <a href="javascript:void(0)" class="blue"><span class="lighter_blue"><i class="fab fa-facebook-f"> </i> <i class="fab fa-youtube-square ml-3"></i></span></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="bg-vide section-xl slide_section" data-vide-bg="mp4:<?php echo get_theme_root_uri()?>/test/assets/video.mp4, poster: <?php echo get_theme_root_uri()?>/test/assets/video.mp4" data-vide-options="posterType: jpg, position: 0% 50%">
        <div style="position: absolute; z-index: -1; top: 0px; left: 0px; bottom: 0px; right: 0px; overflow: hidden; background-size: cover; background-color: transparent; background-repeat: no-repeat; background-position: 0% 50%; background-image: none;">
            <video autoplay loop muted playsinline style="margin: auto; position: absolute; z-index: -1; top:0; left: 0%;/* transform: translate(0%, -50%);*/ visibility: visible; opacity: 1; width: 100%; height: auto;" id="my_video">
                <source src="<?php echo get_theme_root_uri()?>/test/assets/video.mp4" type="video/mp4">
                <source src="<?php echo get_theme_root_uri()?>/test/assets/video.webm" type="video/webm">
            </video>
        </div>
        <div class="container container-wide">
            <div class="row row-50 justify-content-md-center">
                <div class="col-md-10 col-xl-9">
                </div>
            </div>
        </div>
    </div>
    <div class="header">
        <div class="xs_bg_blue bg_blue">
            <nav class="nav navbar-nav relative z_index navbar-expand-lg">
                <div class="container">
                    <div class="row justify-content-around">
                        <div class="col-sm-12 col-lg-3">
                            <div class="row justify-content-around align-items-center">
                                <div class="col-4 col-sm-4 col-lg-12 pl-3">
                                    <img src="<?php echo get_theme_root_uri()?>/test/assets/img/logo.png" class="img-fluid" alt="" />
                                </div>
                                <div class="col-6 col-sm-2 text-right p-2">
                                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                        <i class="fas fa-bars text-light"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-9 text-right">
                            <div class="row justify-content-end">
                                <div class="collapse navbar-collapse text-right col-10" id="navbarSupportedContent">
                                    <ul class="navbar-nav mr-auto w-100 justify-content-end">
                                        <li class="nav-item"><a href="javascript:void(0)" class="nav-link pr-0">ABOUT US</a></li>
                                        <li class="nav-item"><a href="javascript:void(0)" class="nav-link pr-0">BUY</a></li>
                                        <li class="nav-item"><a href="javascript:void(0)" class="nav-link pr-0">SELL</a></li>
                                        <li class="nav-item"><a href="javascript:void(0)" class="nav-link pr-0">OUR TEAM</a></li>
                                        <li class="nav-item"><a href="javascript:void(0)" class="nav-link pr-0">CAREERS</a></li>
                                        <li class="nav-item"><a href="javascript:void(0)" class="nav-link pr-0">CONTACT US</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="container lg-pt-5">
                <div class="jcc row lg-pt-5 xs-pb-5">
                    <h1 class="text-light title lg-pb-5">#TakeMeHome</h1>
                    <div class="input-group lg-pt-5 col-sm-10 col-8">
                        <div class="input-group-prepend marker pt-1">
                            <span><i class="fas fa-map-marker-alt"></i></span>
                        </div>
                        <input type="text" class="form-control search" placeholder="Start your search by City...">
                        <div class="input-group-append">
                            <button class="btn btn-warning text-light" id="search" type="button">Search</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid xs-mt_-70">
                <div class="row align-items-end justify-content-center  xs-pt-5">
                    <img src="<?php echo get_theme_root_uri()?>/test/assets/img/curves2.png" class="img-fluid w-100 mt_m140" alt="">
                    <div class="listings h-100 xs-relative xs-mt_-40" id="about_section">
                        <div class="row align-items-end justify-content-center xs-bg-lighter h-100">
                            <div class="col-sm-10">
                                <div class="row">
                                    <div class="col-md-4 ml-2 pl-5 col-lg-4 col-xl-3 offset-xl-1 light">
                                        <div class="col-12 pl-0">
                                            <div class="circle small"></div>
                                            <div class="circle big"></div>
                                        </div>
                                        <div class="col-12 pl-0">
                                            <h1 class="blue main_title title">ABOUT</h1>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.  ptio perspiciatis praesentium quia quibusdam quidem quos recusandae sint tempora voluptatum.</p>
                                    </div>
                                    <div class="col-sm-12 col-12 col-md-7 offset-xl-2 col-xl-6">
                                        <div class="row justify-content-md-end xs-hidden">
                                            <div class="inl_block relative yellow_btn">
                                                <img src="<?php echo get_theme_root_uri()?>/test/assets/img/now_yellow.png" class="img-fluid" style="border-radius: 17px" alt="">
                                                <span class="now">now</span>
                                                <h4 class="blue an_btn text-center yellow_btn">REQUEST A SHOWING</h4>
                                                <div class="row justify-content-center justify-content-end animation_div yellow_btn">
                                                    <img src="<?php echo get_theme_root_uri()?>/test/assets/img/arr_right.png" class="img-fluid animation_arrow" alt="">
                                                </div>
                                            </div>
                                            <div class="inl_block relative blue_btn">
                                                <img src="<?php echo get_theme_root_uri()?>/test/assets/img/now_blue.png" class="img-fluid" style="border-radius: 17px" alt="">
                                                <span class="now">now</span>
                                                <h4 class="blue an_btn text-center blue_btn">SELL MY HOUSE</h4>
                                                <div class="row justify-content-end animation_div blue_btn">
                                                    <img src="<?php echo get_theme_root_uri()?>/test/assets/img/arr_right.png" class="img-fluid animation_arrow" alt="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row justify-content-around xs-flex xs-pb-5">
                                            <button class="btn btn-outline-warning xs_now">REQUEST A SHOWING</button>
                                            <button class="btn btn-outline-primary xs_now">SELL MY HOUSE</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="listings_section" class="xs-pt-10p xs-pb-5">
    <div class="outlined_circle_xs left"></div>
    <img src="<?php echo get_theme_root_uri()?>/test/assets/img/bg-2.png" class="img-fluid w-100 xs-hidden" alt="">
    <div class="listings bg_img xs-relative xs-pb-5" id="">
        <div class="container-fluid pt_15">
            <div class="row jcc">
                <div class="col-sm-10 ml-2 pl-5 sm-pl-5">
                    <div class="col-12 pl-0">
                        <div class="circle small"></div>
                        <div class="circle big"></div>
                    </div>
                    <div class="col-12 pl-0">
                        <h1 class="main_title title text-light">LISTINGS</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container lg-pb-5">
            <div class="row jcc">
                <div class="col-sm-12 col-md-4 lg-pt-5 bounceInLeft">
                    <div class="col-sm-10 text-center bg-light pr-0 mb-5 pl-0 m- items_div ">
                        <div class="row">
                            <div class="col-md-12 col-6 col-sm-6">
                                <a href="javascript:void(0)" class="items_link">
                                    <img src="<?php echo get_theme_root_uri()?>/test/assets/img/item1.png" class="img-fluid" alt="">
                                </a>
                            </div>
                            <div class="col-md-12 col-5 col-sm-5">
                                <div class="row h-100 jcc">
                                    <h6 class="pl-4 pr-4 pt-2 pb-2">419 Pleasant St. lowa City,lowa 52245 $449.90</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 lg-pt-5 bounceInDown">
                    <div class="col-sm-10 text-center bg-light pr-0 mb-5 pl-0 m- items_div ">
                        <div class="row flex-row-reverse">
                            <div class="col-md-12 col-6 col-sm-6">
                                <a href="javascript:void(0)" class="items_link">
                                    <img src="<?php echo get_theme_root_uri()?>/test/assets/img/item2.png" class="img-fluid" alt="">
                                </a>
                            </div>
                            <div class="col-md-12 col-5 col-sm-5">
                                <div class="row h-100 jcc">
                                    <h6 class="pl-4 pr-4 pt-2 pb-2">419 Pleasant St. lowa City,lowa 52245 $449.90</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 lg-pt-5 bounceInRight">
                    <div class="col-sm-10 text-center bg-light pr-0 mb-5 pl-0 m- items_div ">
                        <div class="row">
                            <div class="col-md-12 col-6 col-sm-6">
                                <a href="javascript:void(0)" class="items_link">
                                    <img src="<?php echo get_theme_root_uri()?>/test/assets/img/item3.png" class="img-fluid" alt="">
                                </a>
                            </div>
                            <div class="col-md-12 col-5 col-sm-5">
                                <div class="row h-100 jcc">
                                    <h6 class="pl-4 pr-4 pt-2 pb-2">419 Pleasant St. lowa City,lowa 52245 $449.90</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="slide_section">
    <img src="<?php echo get_theme_root_uri()?>/test/assets/img/bg-3.png" class="img-fluid w-100 xs-hidden" alt="">
    <div class="outlined_circle"></div>
    <div class="community listings h-100 xs-relative xs-pt-5">
        <div class="bg_blue h-100">
            <div class="container-fluid h-100">
                <div class="row jcc align-items-center h-100">
                    <div class="col-sm-10 ml-2 pl-5 sm-pl-5">
                        <div class="col-12 pl-0">
                            <div class="circle small"></div>
                            <div class="circle big"></div>
                        </div>
                        <div class="col-12 pl-0 z_index">
                            <h1 class="main_title text-light">COMMUNITIES</h1>
                        </div>
                    </div>
                    <div class="col-sm-10">
                        <div class="row jcc align-items-center h-100 xs-pt-5 xs-pb-5 xs-hidden">
                            <div class="col-sm-2 col-2 bounce_btn text-center">
                                <div class="row jcc">
                                    <button class="bg-transparent btn custom_btn left_btn"><img src="<?php echo get_theme_root_uri()?>/test/assets/img/arr_left.png" class="img-fluid arrow_left" alt=""></button>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="row jcc">
                                    <div class="col-sm-7 col bounce_big text-center">
                                        <img src="<?php echo get_theme_root_uri()?>/test/assets/img/slide_l.png" class="img-fluid img-bordered" alt="">
                                    </div>
                                    <div class="col-sm-5 xs-hidden">
                                        <div class="row">
                                            <div class="col-sm-12 pb-3 bounce_small"><img src="<?php echo get_theme_root_uri()?>/test/assets/img/slide2_t.png" class="img-fluid img-bordered" alt=""></div>
                                            <div class="col-sm-12 bounce_small"><img src="<?php echo get_theme_root_uri()?>/test/assets/img/slide3.png" class="img-fluid img-bordered" alt=""></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 col-2 bounce_btn text-center">
                                <div class="row jcc">
                                    <button class="bg-transparent btn custom_btn right_btn"><img src="<?php echo get_theme_root_uri()?>/test/assets/img/arr_right.png" class="img-fluid arrow_right" alt=""></button>
                                </div>
                            </div>
                        </div>
                        <div class="row jcc xs-flex">
                            <div class="col-6">
                                <div class="owl-carousel" id="com_owl">
                                    <div><img src="<?php echo get_theme_root_uri()?>/test/assets/img/slide_l.png" class="img-fluid img-bordered" alt=""></div>
                                    <div><img src="<?php echo get_theme_root_uri()?>/test/assets/img/slide2.png" class="img-fluid img-bordered" alt=""></div>
                                    <div><img src="<?php echo get_theme_root_uri()?>/test/assets/img/slide3.png" class="img-fluid img-bordered" alt=""></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="project_section">
    <!-- <div class="outlined_circle_xs bottom bg_f"></div> -->
    <img src="<?php echo get_theme_root_uri()?>/test/assets/img/bg-4.png" class="img-fluid w-100 xs-hidden" alt="">
    <div class="listings lg-h-100 xs-relative">
        <div class="container-fluid h-100">
            <div class="row lg-h-100 align-items-center jcc pb-5">
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-4 col-lg-3">
                            <div class="col-sm-10 light pr-0 pl-0 sm-pl-5 pt-3">
                                <div class="col-12 pl-0">
                                    <div class="circle small"></div>
                                    <div class="circle big"></div>
                                </div>
                                <div class="col-12 pl-0">
                                    <h3 class="main_title title blue">FEATURED PROJECT</h3>
                                </div>
                            </div>
                            <div class="col-sm-12 lg-pt-5 pr-0 pl-0 sm-pl-5">
                                <h4 class="blue">PROJECT NAME</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
                                <button class="btn btn-warning text-light bordered_btn">KNOW MORE</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="listings bg_img h-100 xs-hidden" style="background-image: url('<?php echo get_theme_root_uri()?>/test/assets/img/bg-house.png');-webkit-background-size: cover;background-size: cover;background-position: center center;z-index: -1;background-attachment: fixed;">

    </div>
</section>
<section class="team">
    <div class="outlined_circle_xs team_top bg_b"></div>
    <div class="outlined_circle_xs team_bottom"></div>
    <img src="<?php echo get_theme_root_uri()?>/test/assets/img/bg-5.png" class="img-fluid w-100 xs-hidden" alt="">
    <div class="listings team bg_img h-100 md-pb-5 xs-relative xs-pt-5">
        <div class="container h-100 md-pb-5 mb-5 xs-pt-5 xs-pb-5">
            <div class="row justify-content-center pt-5 pl-0 pr-0">
                <div class="col-md-9 col-sm-6 col-6">
                    <div class="row justify-content-center align-items-center pb-5 h-100">
                        <div class="owl-carousel" data-nav="true" id="team_owl">
                            <div>
                                <img src="<?php echo get_theme_root_uri()?>/test/assets/img/team1.png" class="img-fluid img-bordered" alt="" style="" >
                            </div>
                            <div>
                                <img src="<?php echo get_theme_root_uri()?>/test/assets/img/team2.png" class="img-fluid img-bordered" alt="" style="" >
                            </div>
                            <div>
                                <img src="<?php echo get_theme_root_uri()?>/test/assets/img/team3.png" class="img-fluid img-bordered" alt="" style="" >
                            </div>
                            <div>
                                <img src="<?php echo get_theme_root_uri()?>/test/assets/img/team4.png" class="img-fluid img-bordered" alt="" style="" >
                            </div>
                            <div>
                                <img src="<?php echo get_theme_root_uri()?>/test/assets/img/team3.png" class="img-fluid img-bordered" alt="" style="" >
                            </div>
                            <div>
                                <img src="<?php echo get_theme_root_uri()?>/test/assets/img/team4.png" class="img-fluid img-bordered" alt="" style="" >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="get" id="get_section">
    <img src="<?php echo get_theme_root_uri()?>/test/assets/img/bg-6.png" class="img-fluid w-100 xs-hidden" alt="">
    <div class="outlined_circle left"></div>
    <!-- style="background-image: url('img/bg-6.png');-webkit-background-size: 100%;background-size: 100%;"-->
    <div class="listings lg-h-100 xs-relative xs-pb-5">
        <div class="container-fluid lg-h-100">
            <div class="row align-items-center xs_jcc lg-h-100">
                <div class="col-sm-4 col-10 offset-md-1 mt-3">
                    <div class="col-sm-12 pl-0 pb-3">
                        <div class="col-12 pl-0">
                            <div class="circle small"></div>
                            <div class="circle big"></div>
                        </div>
                        <div class="col-12 pl-0">
                            <h1 class="main_title text-light">GET IN TOUCH</h1>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput1" class="text-light">Full Name</label>
                        <input type="email" class="form-control bordered_inp form-control-lg" id="exampleFormControlInput1" placeholder="e.i. Carib DAniels">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput2" class="text-light">Email address</label>
                        <input type="email" class="form-control bordered_inp form-control-lg" id="exampleFormControlInput2" placeholder="e.i. email@email.com">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput3" class="text-light">Subject</label>
                        <input type="email" class="form-control bordered_inp form-control-lg" id="exampleFormControlInput3" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput3" class="text-light">Message</label>
                        <div class="input-group">
                            <textarea type="text" class="form-control bordered_inp textarea" placeholder="Message" aria-label="Recipient's username" aria-describedby="basic-addon2"></textarea>
                            <div class="input-group-append">
                                <button class="btn btn-warning text-light textarea_btn" type="button">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="footer_section">
    <div class="footer" style="background-image: url('<?php echo get_theme_root_uri()?>/test/assets/img/bg_footer.png');-webkit-background-size: cover;background-position: center;background-size: cover;">
        <div class="container lg-pt-5 ">
            <div class="row md-pb-5 lg-pt-5">
                <div class="col-sm-4 col-6">
                    <img src="<?php echo get_theme_root_uri()?>/test/assets/img/logo-footer.png" class="img-fluid" alt="">
                </div>
                <div class="col-sm-3 col-6">
                    <ul class="list-style-none">
                        <li><a href="javascript:void(0)" class="text-light">ABOUT US</a></li>
                        <li><a href="javascript:void(0)" class="text-light">BUY</a></li>
                        <li><a href="javascript:void(0)" class="text-light">SELL</a></li>
                        <li><a href="javascript:void(0)" class="text-light">OUR TEAM</a></li>
                        <li><a href="javascript:void(0)" class="text-light">CAREERS</a></li>
                        <li><a href="javascript:void(0)" class="text-light">CONTACT US</a></li>
                    </ul>
                </div>
                <div class="col-sm-5 col-12">
                    <div class="row">
                        <div class="col-8">
                            <p class="text-light"><i class="fas fa-map-marker-alt mr-2"></i> <span>1918 St Andrews CT NE ,Cedar Rapids, IA ,52402</span></p>
                            <p class="text-light"><i class="fas fa-phone mr-2"></i> <a href="tel:(319)3930900" class="text-light"><span>(319)</span>393-0900</a></p>
                            <p class="text-light"><i class="fas fa-location-arrow mr-2"></i>  <a href="mailto:info@pinnaclerealtyia.com" class="text-light"><span>info@</span>pinnaclerealtyia.com</a></p>
                        </div>
                        <div class="col-4">
                            <div>
                                <button class="btn bordered_8 blue fb"><i class="fab fa-facebook-f"> </i> </button>
                            </div>
                            <div class="mt-1">
                                <button class="btn bordered_8 blue"><i class="fab fa-youtube "></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row lg-pt-5">
                <div class="col-sm-6 col-12">
                    <p class="text-light sm-font">© Copyright 2018 - All Rights Reserved - Pinnacle Reality</p>
                </div>
                <div class="col-sm-6 col-12 md-text-right">
                    <span class="text-light sm-font">Website design,SEO,& Online Marketing by Dotcom Design</span>
                </div>
            </div>
        </div>
    </div>
</section>
<button class="bg-transparent btn custom_btn to_top">
    <img src="<?php echo get_theme_root_uri()?>/test/assets/img/arr_top.png" class="img-fluid arrow_top" alt="">
</button>
<script src="<?php echo get_theme_root_uri()?>/test/assets/js/test_script.js" type="text/javascript"></script>
<script>
    $(function(e){
        $('#com_owl').owlCarousel({
            items:1,
            singleItem: true,
            loop:true,
            margin:20,
            autoplay:true,
            dots:true,
            autoplayTimeout:4000,
            autoplayHoverPause:true,
            nav:true,
            navText: ['<img src="<?php echo get_theme_root_uri()?>/test/assets/img/arr_left.png" class="img-fluid arrow_left" alt="">', '<img src="<?php echo get_theme_root_uri()?>/test/assets/img/arr_right.png" class="img-fluid arrow_right" alt="">']
        })
        if($(window).innerWidth()>771){
            var items = 4

        }else{
            var items = 1
            
// After destory, the markup is still not the same with the initial.
// The differences are:
//   1. The initial content was wrapped by a 'div.owl-stage-outer';
//   2. The '.owl-carousel' itself has an '.owl-loaded' class attached;
//   We have to remove that before the new initialization.
           

        }

        $('#team_owl').owlCarousel({
            items:items,
            singleItem: true,
            loop:true,
            margin:20,
            autoplay:true,
            dots:true,
            autoplayTimeout:2000,
            autoplayHoverPause:true,
            nav:true,
            navText: ['<img src="<?php echo get_theme_root_uri()?>/test/assets/img/arr_left.png" class="img-fluid arrow_left" alt="">', '<img src="<?php echo get_theme_root_uri()?>/test/assets/img/arr_right.png" class="img-fluid arrow_right" alt="">']
        })
    })
    $(window).on('resize load',function () {
    	var $owl = $('#com_owl');
            $owl.trigger('destroy.owl.carousel');
    	 $owl.html($owl.find('.owl-stage-outer').html()).removeClass('owl-loaded');
            $owl.owlCarousel({
                items:1,
                singleItem: true,
                loop:true,
                margin:20,
                autoplay:true,
                dots:true,
                autoplayTimeout:4000,
                autoplayHoverPause:true,
                nav:true,
                navText: ['<img src="<?php echo get_theme_root_uri()?>/test/assets/img/arr_left.png" class="img-fluid arrow_left" alt="">', '<img src="<?php echo get_theme_root_uri()?>/test/assets/img/arr_right.png" class="img-fluid arrow_right" alt="">']
            });
        console.log($(window).innerWidth())
        if(+$(window).innerWidth()>750){
            var items = 4
        }else {
            var items = 1
        }
        var $owl = $('#team_owl');
        $owl.trigger('destroy.owl.carousel');
// After destory, the markup is still not the same with the initial.
// The differences are:
//   1. The initial content was wrapped by a 'div.owl-stage-outer';
//   2. The '.owl-carousel' itself has an '.owl-loaded' class attached;
//   We have to remove that before the new initialization.
        $owl.html($owl.find('.owl-stage-outer').html()).removeClass('owl-loaded');
        $owl.owlCarousel({
            items:items,
            singleItem: true,
            loop:true,
            margin:20,
            autoplay:true,
            dots:true,
            autoplayTimeout:2000,
            autoplayHoverPause:true,
            nav:true,
            navText: ['<img src="<?php echo get_theme_root_uri()?>/test/assets/img/arr_left.png" class="img-fluid arrow_left" alt="">', '<img src="<?php echo get_theme_root_uri()?>/test/assets/img/arr_right.png" class="img-fluid arrow_right" alt="">']
        });
    })
</script>
</body>
</html>
