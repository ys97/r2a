$(window).on('scroll',function () {
    if ($(window).scrollTop() >= $('#slide_section').offset().top-($(window).innerHeight()-100)) {
        $('.community').addClass('run')
    }else{
        $('.community').remove('run')
    }
    if ($(window).scrollTop() >= $('#get_section').offset().top-($(window).innerHeight())) {
        $('#get_section').addClass('run')
    }else{
        $('#get_section').remove('run')
    }
    if ($(window).scrollTop() >= $('#listings_section').offset().top-($(window).innerHeight()/2)) {
        $('#listings_section').addClass('run')
    }else{
        $('#listings_section').remove('run')
    }
    if ($(window).scrollTop() >= 100) {
        $('#about_section').addClass('run')
        $('.yellow_btn').show()
        $('.blue_btn').show()
    }else{
        $('#about_section').remove('run')
    }
})
$(window).on('scroll',function () {
    if($(this).scrollTop() > 450){
        $('.to_top').addClass('active')
    }else{
        $('.to_top').removeClass('active')
    }
})
$('.to_top').on('click',function () {
    var body = $("html, body");
    body.animate({scrollTop:0}, 500);
})