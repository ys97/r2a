<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

</div><!-- #content -->
</div><!-- .site-content-contain -->
</div><!-- #page -->
<footer>
    <section class="section page-footer page-footer-default text-left bg-gray-darker" id="footer_section">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-10 col-xl-10">
                    <div class="row">
                        <div class="col-md-4 mt_10" style="min-height: 250px">
                            <?php
                            if ( is_active_sidebar( 'instagram_feed' ) ) {
                                dynamic_sidebar( 'instagram_feed' );
                            }?>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-6">
                                    <div class="row justify-content-center">
                                        <?php if ( is_active_sidebar( 'contact_info' ) ) {
                                            dynamic_sidebar( 'contact_info' );
                                        } ?>
                                    </div>
                                    <div class="row socials  justify-content-center">
                                        <div class="col-md-12 text-center">
                                            <?php wp_nav_menu( array(
                                                'theme_location' => 'social',
                                                'menu_class'     => 'social-links-menu',
                                                'depth'          => 1,
                                                'link_before'    => '<span class="screen-reader-text">',
                                                'link_after'     => '</span>' . twentyseventeen_get_svg( array( 'icon' => 'chain' ) ),
                                            ) ); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <?php if ( is_active_sidebar( 'facebook_feed' ) ) {
                                        dynamic_sidebar( 'facebook_feed' );
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <?php if ( is_active_sidebar( 'copyright' ) ) {
                dynamic_sidebar( 'copyright' );
            } ?>
        </div>
    </section>
</footer>

<a href="javascript:void(0)" id="ui-to-top" class="ui-to-top"></a>
<?php wp_footer(); ?>
<script>
    $(function() {
        let footer_h = $('#footer_section').outerHeight(),
            header_h = $('.navigation-top').outerHeight(),
            window_h = $(window).outerHeight(),
            h;
        if ($(window).width() > 1090) {
            h = window_h-footer_h-header_h;
        }else{
            h = window_h-footer_h;
        }
        $('#page').css('min-height',h)
    })
</script>
</body>
</html>
