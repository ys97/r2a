<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<!-- Top slider-->
<section class="section bg-default text-center mb_40">
    <?php
        $arg_slide = array('post_type'=>'top_slider','posts_per_page'=>-1);
        $slides = new WP_Query($arg_slide);
        if($slides->have_posts()):?>
            <div class="top_slider">
        <!-- <div class="owl-carousel top_slider" data-autoplay="true" data-items="1" data-stage-padding="0" data-loop="false" data-margin="0" data-mouse-drag="true" data-dots="false"> -->
        <?php while($slides->have_posts()):$slides->the_post();
            if(get_field('video')):?>
            <div class="bg-vide section-xl slide_section" data-vide-bg="mp4:<?php echo the_field('video')?>, poster: <?php echo the_field('video')?>" data-vide-options="posterType: jpg, position: 0% 50%">
                    <div style="position: absolute; z-index: -1; top: 0px; left: 0px; bottom: 0px; right: 0px; overflow: hidden; background-size: cover; background-color: transparent; background-repeat: no-repeat; background-position: 0% 50%; background-image: none;">
                        <video autoplay="true" loop="true" muted="true" style="margin: auto; position: absolute; z-index: -1; top: 50%; left: 0%; transform: translate(0%, -50%); visibility: visible; opacity: 1; width: 100%; height: auto;">
                            <source src="<?php echo the_field('video')?>" type="video/mp4">
                        </video>
                    </div>
                    <div class="container container-wide">
                        <div class="row row-50 justify-content-md-center">
                            <div class="col-md-10 col-xl-9">
                                <?php the_field('description');
                                if(get_field('link_text')):?>
                                <a class="button button-primary" href="<?php the_field('link_url')?>"><?php the_field('link_text')?></a>
                            <?php endif;?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php elseif(get_field('iframe_video')): ?>
                <div class="bg-vide">
                    <iframe src="http://www.youtube.com/embed/<?= get_field('iframe_video');?>" width="100%" height="600" frameborder="0"></iframe>
                </div>
            <?php elseif(get_field('image')): ?>

                <div class="slide_section">
                    <div style="position: absolute; z-index: -1; top: 0px; left: 0px; bottom: 0px; height:600px;right: 0px; overflow: hidden; background-size: cover; background-color: transparent; background-repeat: no-repeat; background-position: 0% 50%; background-image: url(<?= get_field('image');?>);">
                    </div>
                    <div class="container container-wide" >
                        <div class="row row-50 justify-content-center align-items-center" style="height: 600px">
                            <div class="col-md-10 col-xl-9">
                                <?php the_field('description');
                                if(get_field('link_text')):?>
                                <a class="button button-primary" href="<?php the_field('link_url')?>"><?php the_field('link_text')?></a>
                            <?php endif;?>
                            </div>
                        </div>
                    </div>
                </div>
        <?php endif;?>
    <?php endwhile;?>
    </div>
<?php endif;?>
</section>

<!--Our Services-->
<?php $arg_services = array(
    'post_type'=>'our_services',
    'posts_per_page'=>-1,
);
$services = new WP_Query($arg_services);
if($services->have_posts()):?>
    <section class="section section-lg bg-default text-center" id="services">
        <div class="container-wide">
            <div class="row justify-content-center mb_20">
                <div>
                    <h3><?php echo __('Our Services','Home Page')?></h3>
                    <hr class="divider divider-default">
                </div>
            </div>
            <div class="row row-50 justify-content-center offset-custom-2">
                <?php while($services->have_posts()):$services->the_post(); ?>
                    <div class="col-10 col-sm-10 col-lg-4 col-xl-4">
                        <div class="thumbnail-classic unit flex-md-row flex-lg-column flex-column thumbnail-classic-primary">
                            <div class="thumbnail-classic-icon unit-left"><?=the_field('icon');?></div>
                            <div class="thumbnail-classic-caption unit-body ml_0 pl_0">
                                <h6 class="thumbnail-classic-title"><?php the_title();?></h6>
                                <hr class="divider divider-default divider-sm">
                                <p><?= get_field('description');?></p>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </section>
<?php endif;?>

<?php
    $arg_team = array(
    'post_type'=>'team',
    'posts_per_page'=>-1
    );
    $team = new WP_Query($arg_team);
    if($team->have_posts()):?>

<!--Our Team-->

<section class="section section-lg bg-default" id="team">
    <div class="container-wide">
        <div class="row justify-content-center">
            <div>
                <h3><?php echo __('Our Team','Home Page')?></h3>
                <hr class="divider divider-default">
            </div>
        </div>
        <!-- Owl Carousel-->
        <div class="owl-carousel owl-carousel-team" data-autoplay="true" data-items="1" data-md-items="2" data-lg-items="3" data-xl-items="4" data-xxl-items="6" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="false" data-dots="true">
            <?php
                while($team->have_posts()):$team->the_post();
                    if(get_field('image')):
                        $image = get_field('image');
                        $url = $image['url'];
                        $title = $image['title'];
                        $alt = $image['alt'];
                        $caption = $image['caption'];

                        // thumbnail
                        $size = 'medium';
                        $thumb = $image['sizes'][$size];
                        $width = $image['sizes'][$size . '-width'];
                        $height = $image['sizes'][$size . '-height'];
                    endif; ?>
                    <div class="team-classic">
                        <div class="team-classic-image">
                            <figure><?php if(get_field('image')):?><img src="<?= $thumb?>" alt="" width="270" height="270"/><?php endif;?></figure>
                            <div class="team-classic-image-caption">
                                <ul class="list-inline list-team">
                                    <?php if(get_field('facebook')):?>
                                        <li><a class="icon icon-facebook" href="<?= get_field('facebook')?>" target="_blank"><i class="fa fa-facebook-f"></i></a></li>
                                    <?php endif;?>
                                    <?php if(get_field('twitter')):?>
                                        <li><a class="icon icon-twitter" href="<?= get_field('twitter')?>"><i class="fab fa-twitter"></i></a></li>
                                    <?php endif;?>
                                    <?php if(get_field('instagram')):?>
                                        <li><a class="icon icon-instagram" href="<?= get_field('instagram')?>"><i class="fab fa-instagram"></i></a></li>
                                    <?php endif;?>
                                    <?php if(get_field('gmail')):?>
                                        <li><a class="icon icon-google-plus" href="<?= get_field('gmail')?>"><i class="fab fa-google-plus-g"></i></a></li>
                                    <?php endif;?>
                                    <?php if(get_field('linkedin')):?>
                                        <li><a class="icon icon-linkedin" href="<?= get_field('linkedin')?>"><i class="fab fa-linkedin-in"></i></a></li>
                                    <?php endif;?>
                                </ul>
                            </div>
                        </div>
                        <div class="team-classic-caption">
                            <h4><a class="team-classic-title" href="javascript:void(0)"><?php the_title();?></a></h4>
                            <p class="team-classic-job-position"><?php the_field('position');?></p>
                        </div>
                    </div>
                <?php endwhile; ?>
        </div>
        <div class="row justify-content-center mb_20">
            <a class="button button-primary" href="/about_us"><?php echo __('view all team','Home Page')?></a>
        </div>
    </div>
</section>
<?php endif;?>


<?php $arg_adv = array(
    'post_type'=>'advantages',
    'posts_per_page'=>-1
);
$advantages = new WP_Query($arg_adv);
if($advantages->have_posts()):?>

<!-- Our Advantages-->
<section class="section section-wrap section-wrap-lg section-reverse bg-gray-darker" id="advantages">
    <div class="section-wrap-inner">
        <div class="container container-wide">
            <div class="row row-50 justify-content-md-center  justify-content-xl-end">
                <div class="col-md-12 col-lg-7 col-xl-7 col-xxl-6">
                    <div class="section-lg">
                        <div class="row justify-content-center mb_20">
                            <div>
                                <h3><?php echo __('Our Advantages','Home Page')?></h3>
                                <hr class="divider divider-default">
                            </div>
                        </div>
                        <div class="row row-30">
                            <?php while($advantages->have_posts()):$advantages->the_post();?>
                                <div class="col-md-6">
                                    <article class="box-minimal box-minimal-border">
                                        <div class="box-minimal-icon mdi mdi-thumb-up-outline">
                                            <?= the_field('icon');?>
                                        </div>
                                        <p class="big box-minimal-title"><?php the_title();?></p>
                                        <hr>
                                        <div class="box-minimal-text"><?= get_field('description');?></div>
                                    </article>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-wrap-aside section-wrap-image"><img src="<?= esc_url(get_template_directory_uri());?>/assets/images/landing.jpg" alt="" width="942" height="1250"/>
        </div>
    </div>
</section>
<?php endif;?>

    <!--Counters-->
<?php $arg_counters = array(
    'post_type'=>'counters',
    'posts_per_page'=>-1
);
$counters = new WP_Query($arg_counters);
if($counters->have_posts()):?>
    <section class="section section-lg parallax-container " id="counter_section">
        <div class="material-parallax" style="background-image: url(<?php echo esc_url(get_template_directory_uri())?>/assets/images/parallax-image-1.jpg);background-attachment: fixed;"></div>
        <div class="parallax-content">
            <div class="section-md text-center">
                <div class="container">
                    <div class="row row-50 justify-content-sm-center">
                        <?php while($counters->have_posts()):$counters->the_post()?>
                            <div class="col-sm-6 col-md-3 col-xl-3 counter_main">
                                <div class="counter-wrap"><span class="icon icon-primary mdi mdi-television-guide"></span>
                                    <div class="circle_holder">
                                        <div class="counters_icon">
                                            <div class="heading-4">
                                                <span class="counter" data-step="3000"><?php the_field('count');?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <p><?php the_title();?></p>
                                </div>
                            </div>
                        <?php endwhile;?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif;?>

    <!--Contact US-->
<section class="section section-lg bg-default" id="contacts_section">
    <div class="container-wide">
        <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-7 col-xl-7 ">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d453.1325761799027!2d44.51082523838396!3d40.180126106897156!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf62c3a4db3402d4b!2sR2A+Development+Company!5e0!3m2!1sru!2s!4v1543574754661" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="col-md-5 col-xl-5">
                    <?php
                    if(ICL_LANGUAGE_CODE == 'en'){
                        echo do_shortcode('[contact-form-7 id="42" title="Contact Us Form"]');
                    }else{
                        echo do_shortcode('[contact-form-7 id="111" title="Contact us Arm"]');
                    }?>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

    <!--Testimonials-->
<?php $arg_testimonials = array(
    'post_type'=>'testimonials',
    'posts_per_page'=>-1
);
$testimonials = new WP_Query($arg_testimonials);
if($testimonials->have_posts()):?>

    <section class="section parallax-container bg-image" id="testimonials">
        <div class="material-parallax" style="background-image: url(<?php echo esc_url(get_template_directory_uri())?>/assets/images/parallax-image-2.jpg);background-attachment: fixed;"></div>
        <div class="parallax-content">
            <div class="section-md text-center">
                <div class="container container-wide">
                    <h3>Testimonials</h3>
                    <hr class="divider divider-default w_100p">
                    <!-- Owl Carousel-->

                    <div class="owl-carousel testimonials_car" data-autoplay="true" data-items="1" data-md-items="2" data-lg-items="3" data-xl-items="3" data-xxl-items="4" data-dots="true" data-nav="false" data-stage-padding="0" data-loop="true" data-margin="30" data-mouse-drag="false">
                        <?php while($testimonials->have_posts()):$testimonials->the_post();
                            if(get_field('image')) {
                                $image = get_field('image');
                                // thumbnail
                                $size = 'thumbnail';
                                $thumb_tes = $image['sizes'][$size];
                                $width = $image['sizes'][$size . '-width'];
                                $height = $image['sizes'][$size . '-height'];
                            }else{
                                $thumb_tes = '';
                            }?>
                        <article class="quote-modern">
                            <div class="quote-modern-text">
                                <p><?php the_field('description')?></p>
                            </div>
                            <div class="quote-modern-footer">
                                <?php if(get_field('image')) {?>
                                    <img class="quote-modern-image" src="<?php echo $thumb_tes?>" alt="" width="80" height="80"/>
                                <?php }?>
                                <div class="quote-modern-meta">
                                    <p class="quote-modern-cite"><?php the_title();?></p>
                                </div>
                            </div>
                        </article>
                        <?php endwhile;?>
                    </div>

                </div>
            </div>
        </div>
    </section>
<?php endif;?>
<script>
    $(function () {
        if($(window).width() > 800){
            var items = 3;
        }else{
            var items = 1;
        }
        $('.owl-carousel-team').owlCarousel({
            items:items,
            navigation: true,
            autoplay:true,
            autoplayTimeout:2000,
            loop:true,
            autoplayHoverPause:true,
        })

        $('.testimonials_car').owlCarousel({
            items:items,
            navigation: true,
            autoplay:true,
            autoplayTimeout:2000,
            loop:true,
            autoplayHoverPause:true,
        })

        // $('.top_slider').owlCarousel({
        //     items:1,
        //     nav:true,
        //     navigation: false,
        //     autoplay:true,
        //     autoplayTimeout:4000,
        //     loop:false,
        //     autoplayHoverPause:true,
        // })
    })
</script>
<?php get_footer();
