<?php
/**
 * Template Name: About
 */
get_header(); ?>

<?php $arg_effective = array(
    'post_type'=>'effective',
    'posts_per_page'=>1,
    'order'=>'DESC',
);
$effective = new WP_Query($arg_effective);
if($effective->have_posts()):
    while($effective->have_posts()):$effective->the_post();
        if(get_field('image')):
            $image = get_field('image');
            $size = 'large';
            $thumb = $image['sizes'][$size];
        endif;?>
<!-- Hi, we are EFFECTIVE-->
<section class="section section-lg bg-gray-lighter">
    <div class="container container-bigger">
        <div class="row row-ten row-50 justify-content-md-center align-items-lg-center justify-content-xl-between flex-lg-row-reverse">
            <div class="col-md-9 col-lg-5 col-xl-4">
                <h3><?php the_title();?></h3>
                <div class="divider divider-default w_100p"></div>
                <p class="heading-5"><?= get_field('description')?></p>
            </div>
            <div class="col-md-9 col-lg-5">
                <img src="<?php echo $thumb?>" alt="" width="720" height="459"/>
            </div>
        </div>
    </div>
</section>

<?php endwhile;endif;?>


<?php
$arg_team = array(
    'post_type'=>'team',
    'posts_per_page'=>-1
);
$team = new WP_Query($arg_team);
if($team->have_posts()):?>
    <!--Our Team-->
<section class="section section-lg bg-gray-lighter">
    <div class="container container-wide">
        <div class="row justify-content-center mb_20">
            <div>
                <h3><?php echo __('Our Team','About us')?></h3>
                <hr class="divider divider-default">
            </div>
        </div>
        <!-- Owl Carousel-->
        <div class="row">
            <?php
            while($team->have_posts()):$team->the_post();
                if(get_field('image')):
                    $image = get_field('image');
                    $url = $image['url'];
                    $title = $image['title'];
                    $alt = $image['alt'];
                    $caption = $image['caption'];

                    // thumbnail
                    $size = 'medium';
                    $thumb = $image['sizes'][$size];
                    $width = $image['sizes'][$size . '-width'];
                    $height = $image['sizes'][$size . '-height'];
                endif; ?>
                <div class="col-md-3 col-sm-6 col-6">
                    <div class="team-classic">
                        <div class="team-classic-image">
                            <figure><img src="<?= $thumb?>" alt="" width="270" height="270"/></figure>
                            <div class="team-classic-image-caption">
                                <ul class="list-inline list-team">
                                    <?php if(get_field('facebook')):?>
                                        <li><a class="icon icon-facebook" href="<?= get_field('facebook')?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                    <?php endif;?>
                                    <?php if(get_field('twitter')):?>
                                        <li><a class="icon icon-twitter" href="<?= get_field('twitter')?>"><i class="fab fa-twitter"></i></a></li>
                                    <?php endif;?>
                                    <?php if(get_field('instagram')):?>
                                        <li><a class="icon icon-instagram" href="<?= get_field('instagram')?>"><i class="fab fa-instagram"></i></a></li>
                                    <?php endif;?>
                                    <?php if(get_field('gmail')):?>
                                        <li><a class="icon icon-google-plus" href="<?= get_field('gmail')?>"><i class="fab fa-google-plus-g"></i></a></li>
                                    <?php endif;?>
                                    <?php if(get_field('linkedin')):?>
                                        <li><a class="icon icon-linkedin" href="<?= get_field('linkedin')?>"><i class="fab fa-linkedin-in"></i></a></li>
                                    <?php endif;?>
                                </ul>
                            </div>
                        </div>
                        <div class="team-classic-caption">
                            <h4><a class="team-classic-title" href="javascript:void(0)"><?php the_title();?></a></h4>
                            <p class="team-classic-job-position"><?php the_field('position');?></p>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>
<?php endif;?>


<?php $arg_adv = array(
    'post_type'=>'advantages',
    'posts_per_page'=>-1
);
$advantages = new WP_Query($arg_adv);
if($advantages->have_posts()):?>
<!-- Our Advantages-->
<section class="section section-lg section-reverse bg-gray-lighter">
    <div class="section-wrap-inner">
        <div class="container container-wide">
            <div class="row row-50 justify-content-md-center">
                <div class="col-md-12 col-lg-7 col-xl-7 col-xxl-6">
                    <div class="section-lg">
                        <div class="row justify-content-center mb_20">
                            <div>
                                <h3><?php echo __('Our Advantages','About us')?></h3>
                                <hr class="divider divider-default">
                            </div>
                        </div>
                        <div class="row row-30">
                            <?php while($advantages->have_posts()):$advantages->the_post();?>
                                <div class="col-md-6">
                                    <article class="box-minimal box-minimal-border">
                                        <div class="box-minimal-icon mdi mdi-thumb-up-outline">
                                            <?= the_field('icon');?>
                                        </div>
                                        <p class="big box-minimal-title"><?php the_title();?></p>
                                        <hr>
                                        <div class="box-minimal-text"><?= get_field('description');?></div>
                                    </article>
                                </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif;?>

<?php get_footer();?>
