<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Top Menu', 'twentyseventeen' ); ?>">
	<div class="navbar_brand">
		<?php the_custom_logo()?>
	</div>
    <button class="menu-toggle" aria-controls="top-menu" aria-expanded="false">
        <?php
        echo twentyseventeen_get_svg( array( 'icon' => 'bars' ) );
        echo twentyseventeen_get_svg( array( 'icon' => 'close' ) );
        ?>
    </button>
	<?php wp_nav_menu( array(
		'theme_location' => 'top',
		'menu_id'        => 'top-menu',
        'container_id'   => 'menu_cont'
	) ); ?>

</nav><!-- #site-navigation -->
