<?php
/**
 * Template Name: Coming soon
 */
?>
<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
<head>
    <!-- Site Title-->
    <title>Local</title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="images/icon.png" sizes="16x16" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Raleway:200,400%7CLato:300,400,300italic,700%7CMontserrat:900">
    <link rel="stylesheet" href="<?php echo get_theme_file_uri("assets/css/cbootstrap.css")?>">
    <link rel="stylesheet" href="<?php echo get_theme_file_uri("assets/css/cstyle.css")?>">
    <link rel="stylesheet" href="<?php echo get_theme_file_uri("assets/css/cfonts.css")?>">
</head>
<body>
<!-- Page preloader-->
<div class="preloader">
    <div class="preloader-body"><img src="<?= get_theme_file_uri('assets/images/logo-default-198x42.png')?>" alt="" width="198" height="42"/>
        <div class="cssload-container">
            <div class="cssload-speeding-wheel"></div>
        </div>
    </div>
</div>
<!-- Page-->
<div class="page">
    <section class="section-single bg-gray-dark">
        <div class="section-single__inner">
            <header class="section page-header">
                <!-- RD Navbar-->
                <div class="rd-navbar-wrap rd-navbar-centered">
                    <nav class="rd-navbar" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-fullwidth" data-xl-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-static" data-md-stick-up-offset="1px" data-lg-stick-up-offset="1px" data-stick-up="true" data-sm-stick-up="true" data-md-stick-up="true" data-lg-stick-up="true">
                        <div class="rd-navbar-inner">
                            <div class="rd-navbar-aside-left">
                                <div class="rd-navbar-nav-wrap">

                                </div>
                            </div>
                            <!-- RD Navbar Panel-->
                            <div class="rd-navbar-panel">
                                <!-- RD Navbar Toggle-->
                                <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                                <!-- RD Navbar Brand-->
                                <div class="rd-navbar-brand" style="padding:50px">
                                    <img class="logo-default" src="<?= get_theme_file_uri('assets/images/logo-default-198x42.png')?>" alt="" width="198" />
                                    <img class="logo-inverse" src="<?= get_theme_file_uri('assets/images/logo-inverse-198x42.png')?>" alt="" width="198" /></div>
                            </div>
                            <div class="rd-navbar-collapse-toggle" data-rd-navbar-toggle=".rd-navbar-collapse"><span></span></div>
                            <div class="rd-navbar-aside-right rd-navbar-collapse">
                                <!-- RD Navbar Search-->

                            </div>
                        </div>
                    </nav>
                </div>
            </header>
            <div class="section-md bg-gray-dark text-center content_section">
                <div class="container-wide">
                    <h4 class="text-regular">We're Getting Ready to Launch In</h4>
                    <div class="countdown-wrap">
                        <div class="DateCountdown" data-type="until" data-date="2019-01-31 00:00:00" data-format="wdhms" data-color="#1cd8c9" data-bg="rgba(255, 255, 255, 0)" data-width="0.02"></div>
                    </div>
                    <p>Our website is coming soon and we are currently working very hard <br class="d-none d-md-inline">to give you the best experience on our new website.</p>
                    <div class="row justify-content-sm-center">
                        <div class="col-md-10 col-xl-5 col-xxl-4">
                            <!-- RD Mailform: Subscribe-->
                            <form class="rd-mailform rd-mailform-inline rd-mailform-sm rd-mailform-inline-modern rd-mailform-inline-centered" data-form-output="form-output-global" data-form-type="subscribe" method="post" action="bat/rd-mailform.php">
                                <div class="rd-mailform-inline-inner">
                                    <div class="form-wrap">
                                        <input class="form-input" type="email" name="email" data-constraints="@Email @Required" id="subscribe-form-email-1"/>
                                        <label class="form-label" for="subscribe-form-email-1">Enter your e-mail</label>
                                    </div>
                                    <button class="button form-button button-sm button-primary" type="submit">Subscribe</button>
                                </div>
                            </form>
                            <p> +374 60 60 60 40<br>Yerevan, Amiryan 4/6,apt.155<br> info@r2adevelopment.am<br> www.r2adevelopment.am</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Page Footer-->
            <div class="section-xs page-footer text-center">
                <div class="container">
                    <p class="right">&#169; <span class="copyright-year"></span> All Rights Reserved
                        &nbsp;<a href="#">Terms of Use</a>&nbsp;<span>and</span>&nbsp;<a href="privacy-policy.html">Privacy Policy</a>
                    </p>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Global Mailform Output-->
<div class="snackbars" id="form-output-global"></div>
<!-- Javascript-->
<script src="<?= get_theme_file_uri('assets/js/core.min.js')?>"></script>
<script src="<?= get_theme_file_uri('assets/js/script.js')?>"></script>
</body><!-- Google Tag Manager --><noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script><!-- End Google Tag Manager -->
</html>