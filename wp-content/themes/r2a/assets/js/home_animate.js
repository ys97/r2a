
var key = 1;
$(window).on('scroll load',function () {
    if($('#services')) {
        if ($(window).scrollTop() > $('#services').offset().top - ($(window).innerHeight()-100)) {
            $('#services').animate({
                'left': '0',
            }, 1300)
        }
    }
    if ($(window).scrollTop() > $('#advantages').offset().top - ($(window).innerHeight()-100)) {
        $('#advantages').animate({
            'left': '0',
        }, 1300)
    }
    if ($(window).scrollTop() > $('#counter_section').offset().top - ($(window).innerHeight()-100)) {
        $('#counter_section').animate({
            'right': '0',
        }, 1300)
        $('.counters_icon').addClass('active')
        if(key == 1) {
            key = 0;
            // $(this).off('scroll');
            $('.counter').each(function () {
                $(this).prop('Counter', 0).animate({
                    Counter: $(this).text()
                }, {
                    duration: 4000,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });

            });  
                    
        }
    }

    if ($(window).scrollTop() > $('#testimonials').offset().top - ($(window).innerHeight()-100)) {
        $(this).on('scroll');
        $('#testimonials').animate({
            'right': '0',
        }, 1300)
        $('#counter_section').data('visible', 0)
    }

})