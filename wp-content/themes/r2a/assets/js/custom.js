$(function(){
    $('.preloader').hide();
    $(window).on('scroll',function () {
        if($(this).scrollTop() > 250){
            $('#ui-to-top').addClass('active')
        }else{
            $('#ui-to-top').removeClass('active')
        }
    })
    $('#ui-to-top').on('click',function () {
        var body = $("html, body");
        body.animate({scrollTop:0}, 500);
    })
    $('.menu-toggle').on('click',function (e) {
        e.preventDefault();
        $('#menu_cont').toggleClass('toggled')
    })
    $('.training_button').on('click',function(){
        $('.wpcf7-hidden').val($(this).data('course'))
        $('#training_form').fadeIn(500)
        $('.backdrop_custom').fadeIn(500);
    })
    $('.backdrop_custom').on('click',function(){
        $('#training_form').fadeOut(500)
        $('.backdrop_custom').fadeOut(500);
    })

})
// $(document).on('keydown',function(e){
//     if (window.event.keyCode == 123 || e.button==2 ){
//         return false;
//     }
//     if(e.ctrlKey){
//         if(e.keyCode == 73 || e.keyCode == 74 || e.keyCode == 85){
//             return false;
//         }
//     }
// });
// $(document).on("contextmenu",function(e) {
//     e.preventDefault();
// });