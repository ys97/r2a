<?php
/**
 * Template Name: Contact
 */
get_header(); ?>
<section class="section section-lg bg-default text-center">
    <div class="container">
        <div class="row row-50 justify-content-sm-center">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-7 col-xl-8">
                    	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d453.1325761799027!2d44.51082523838396!3d40.180126106897156!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf62c3a4db3402d4b!2sR2A+Development+Company!5e0!3m2!1sru!2s!4v1543574754661" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                    <div class="col-md-5 col-xl-4">
                        <?php
                         if(ICL_LANGUAGE_CODE == 'en'){
                             echo do_shortcode('[contact-form-7 id="42" title="Contact Us Form"]');
                         }else{
                             echo do_shortcode('[contact-form-7 id="111" title="Contact us Arm"]');
                         }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer();?>