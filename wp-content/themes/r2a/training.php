<?php
/**
 * Template Name: Training
 */
get_header();
?>
  <?php  $tax = 'training_cats';
    wp_reset_query();
    $args_img = array('post_type' => 'training','posts_per_page' => 1,'order'=>'DESC',
        'tax_query' => array(
        array(
            'taxonomy' => $tax,
            'field' => 'slug',
            'terms' => 'image',
            ),
        ),
    );
    $training_image = new WP_Query($args_img);
    if($training_image->have_posts()){
        while($training_image->have_posts()) {
            $training_image->the_post();
            $image = get_field('image');?>
                <section class="breadcrumbs-custom bg-image" style="background-image: url(<?php echo $image['url'];?>);">
                    <div class="container">
                        <p class="heading-1 breadcrumbs-custom-title"><?php the_title()?></p>
                        <ul class="breadcrumbs-custom-path">
                            <li><a href="/<?php echo ICL_LANGUAGE_CODE == 'en'?'': ICL_LANGUAGE_CODE;?>"><?= __('Home',"Training")?></a></li>
                            <li class="active"><?= __('Training Center',"Training")?></li>
                        </ul>
                    </div>
                </section>
            <?php }}?>
    <?php
    $args_training = array('post_type' => 'training','posts_per_page' => -1,'order'=>'DESC',
        'tax_query' => array(
        array(
            'taxonomy' => $tax,
            'field' => 'slug',
            'terms' => 'courses',
            ),
        ),
    );

    $training_courses = new WP_Query($args_training);
    if($training_courses->have_posts()){?>
        <section class="section section-lg bg-gray-lighter text-center">
            <div class="container">
                <div class="row row-50 justify-content-sm-center">
                    <?php while($training_courses->have_posts()) {
                        $training_courses->the_post();
                        $image = get_field('image');
                        // thumbnail
                        $size = 'thumbnail';
                        $thumb_icon = $image['sizes'][$size];
                        $width = $image['sizes'][$size . '-width'];
                        $height = $image['sizes'][$size . '-height'];?>
                        <div class="col-sm-6 col-md-6 col-lg-3 col-xl-3">
                            <div class="unit flex-lg-column flex-column thumbnail-classic-primary">
                                <div class="courses_icon">
                                    <img src="<?= $thumb_icon;?>" alt="" width="<?= $width?>">
                                </div>
                                <div class="thumbnail-classic-caption unit-body ml_0 pl_0 course_info">
                                    <h6 class="thumbnail-classic-title"><?php the_title();?></h6>
                                    <hr class="divider divider-default divider-sm"/>
                                    <p><?= get_field('description');?></p>
                                </div>
                                <a class="button button-primary training_button" data-course="<?php the_title();?>" href="javascript:void(0)"><?php echo __('register','Training')?></a>
                            </div>
                        </div>
                    <?php }?>
                </div>
            </div>
        </section>
    <?php }?>


<!--Info-->
<?php
$args_info = array('post_type' => 'training','posts_per_page' => -1,'order'=>'ASC',
    'tax_query' => array(
        array(
            'taxonomy' => $tax,
            'field' => 'slug',
            'terms' => 'info',
        ),
    ),
);
$training_info = new WP_Query($args_info);
if($training_info->have_posts()){ $k = 0;?>
    <?php while($training_info->have_posts()) {
        $k++;
        $training_info->the_post();
        $image = get_field('image');
        // thumbnail
        $size = 'large';
        $thumb_info = $image['sizes'][$size];
        $width = $image['sizes'][$size . '-width'];
        $height = $image['sizes'][$size . '-height'];?>
        <section class="section section-lg text-center text-md-left bg-gray-lighter">
            <div class="container container-bigger">
                <div class="row row-ten row-50 justify-content-md-center justify-content-xl-between <?php echo $k%2 == 0?'flex-md-row-reverse':''?>">
                    <div class="col-md-4 col-xxl-3">
                        <h3><?php the_title();?></h3>
                        <div class="divider divider-default w_100p"></div>
                        <p class="heading-5"><?= get_field('description');?></p>
                    </div>
                    <div class="col-md-6">
                        <img src="<?= $thumb_info;?>" alt="" width="870" height="580">
                    </div>
                </div>
            </div>
        </section>
    <?php }?>
<?php }?>


    <div class="col-md-4" id="training_form">
        <?= do_shortcode('[contact-form-7 id="175" title="Courses"]');?>
    </div>
<div class="backdrop_custom"></div>
<?php get_footer();?>