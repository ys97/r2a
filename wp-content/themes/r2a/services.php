<?php
/**
 * Template Name: Services
 */
get_header();?>
<section class="section section-lg bg-default text-center">
    <div class="container-wide">
        <div class="row justify-content-center mb_20">
            <div>
                <h3><?php echo __('Our Services','Services')?></h3>
                <hr class="divider divider-default">
            </div>
        </div>
        <div class="row row-50 justify-content-center offset-custom-2">
            <?php
            $args = array(
                'post_type' => 'our_services',
                'posts_per_page' => -1
            );
            $services = new WP_Query($args);
            if($services->have_posts()):
                while($services->have_posts()):$services->the_post(); ?>
                    <div class="col-10 col-sm-10 col-lg-4 col-xl-4">
                        <div class="thumbnail-classic unit flex-md-row flex-lg-column flex-column thumbnail-classic-primary">
                            <div class="thumbnail-classic-icon unit-left"><?=the_field('icon');?></div>
                            <div class="thumbnail-classic-caption unit-body ml_0 pl_0">
                                <h6 class="thumbnail-classic-title"><?php the_title();?></h6>
                                <hr class="divider divider-default divider-sm"/>
                                <p><?= get_field('description');?></p>
                            </div>
                        </div>
                    </div>
                <?php endwhile;
            endif;?>
        </div>
    </div>
</section>
<?php get_footer()?>
