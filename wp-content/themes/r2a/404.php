<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>
    <div class="section-md bg-default text-center h-100">
        <div class="container h-100">
            <div class="row h-100 justify-content-center align-items-center">
                <div class="col-sm-12"> 
                    <h4 class="text-regular"><?php echo __("Sorry, But the Page Was not Found",'404 page')?></h4>
                    <p class="text-large">404</p>
                    <p class="mb_20"><?php echo __("You may have mistyped the address or the page may have moved.",'404 page')?></p>
                </div>
                <div class="section-single__element">
                    <a class="button button-primary" href="/<?php echo ICL_LANGUAGE_CODE == 'en'?'': ICL_LANGUAGE_CODE;?>"><?php echo __("back to home page",'404 page')?></a>
                </div>
            </div>
        </div>
    </div>
<?php get_footer();
